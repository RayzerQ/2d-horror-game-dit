# README #

* The most important gameplay element to remember is that crouching and sneaking is the way to escape from robots when you've been spotted. Your health bar is in the lower left corner of the screen.

### CONTROLS ###

* WASD - Movement
* C - Crouch
* SPACE BAR - Run


### ACTIONS ###

* The player can move while crouched but its slower. This is how to navigate the area undetected. Alerted robots will lose sight of you if you run and crouch behind an object quick enough

* You are harder to spot while crouching

* Stopping for a moment will restore run stamina, the player can run for 6 seconds. You will regain stamina slower while walking

### THE ROBOTS ###

* Robots have 3 modes - patrol, alert and chase. They will hunt and kill on sight.

* A robot can detect the presence of a human and will enter and stay in alert mode if you are beside one, you must sneak away for it to return to patrol mode

* Escaping at distance or breaking line of sight for a moment or two will return the robot to alert/patrol from chase mode

### THE LEVEL ###

* The level is broken up into differently designed sections to sneak your way through

* There are hidden pathways through each area

* Watch the move patterns of the robots to plot a safe path