﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Robotcontroller : MonoBehaviour {

	[HideInInspector] public float moveHorizontal, moveUpward; 
	public float horizSpeed, verticSpeed, jumpspeed; 
	private bool facingRight = true;
	private bool crouching = false;

	public bool canRunning = false;
	private bool iAmRunning = false;

	public float isRun;
	private float isRunMax = 20f;
	private float isRunMin = 0f;

	public Vector3 currentPos;

	private SpriteRenderer mySpriteRenderer;
	private Animator animator; 
	private CapsuleCollider myCollider;
	private PlayerHealth playerHealthScript;

	public Vector3 capsuleCenter;

	void Awake()
	{
		mySpriteRenderer = GetComponent<SpriteRenderer> ();
		animator = GetComponent<Animator>();
		myCollider = GetComponent<CapsuleCollider> ();
		capsuleCenter = myCollider.center;
		playerHealthScript = GetComponent<PlayerHealth> ();
	}
		
	void Update () 
	{
		animator = GetComponent<Animator>();

		storeAxis ();
		Crouching ();
		MovementAnimation();

		LayerCheck ();

		myCollider.center = capsuleCenter;
	}

	void storeAxis()
	{
		if (crouching == false && iAmRunning == false) 
		{
			horizSpeed = 3.2f;
			verticSpeed = 4f;
			capsuleCenter = new Vector3 (0, 1.05f, 0);
			myCollider.height = 5f;
		}
	
		else if (crouching == true) 
		{
			horizSpeed = 2.5f;
			verticSpeed = 3f;
			capsuleCenter = new Vector3 (0, 0.46f, 0);
			myCollider.height = 3.18f;

		} 
		else if (iAmRunning == true && canRunning == true)
		{
			horizSpeed = 5f;
		}

		if (horizSpeed == 5f) 
		{
			isRun += Time.deltaTime;
		}

		moveHorizontal = Input.GetAxis ("Horizontal") * horizSpeed * Time.deltaTime; // Moves on the X using "A" and "D"
		moveUpward = Input.GetAxis ("Vertical") * verticSpeed * Time.deltaTime; // Moves on the X using "W" and "S"

		if (playerHealthScript.isDead == false) 
		{
			transform.Translate (moveHorizontal, 0, moveUpward);
		}

		if (moveHorizontal > 0 && !facingRight) 
		{ 
			Flip ();
		} 
		else if (moveHorizontal < 0 && facingRight) 
		{
			Flip ();
		}

		if (isRun <= 0f) 
		{
			isRun = isRunMin;
		}

		if (isRun <= isRunMin) 
		{
			canRunning = true;
		} 
		else if (isRun >= isRunMax)
		{
			canRunning = false;
			iAmRunning = false;
		}

	}

	void Crouching()
	{
		if (Input.GetKeyDown (KeyCode.C) && crouching == false && iAmRunning == false) 
		{
			crouching = true;
			animator.SetBool ("PlayerCrouching", true);
			//			animator.SetTrigger ("PlayerCrouch");
		} 
		else if (Input.GetKeyDown (KeyCode.C) && crouching == true && iAmRunning == false) 
		{
			crouching = false;
			animator.SetBool ("PlayerCrouching", false);
			//			animator.SetTrigger ("PlayerStand");
		}
	}

	void MovementAnimation()
	{
		if (playerHealthScript.isDead == true) 
		{
			animator.SetBool ("PlayerDead", true);
			animator.SetBool ("PlayerHit", false);
			animator.SetBool ("PlayerStandB", false);
			animator.SetBool ("PlayerRunning", false);
			animator.SetBool ("PlayerWalkB", false);
		}

		if (playerHealthScript.damaged == true && playerHealthScript.isDead == false) 
		{
			animator.SetBool ("PlayerHit", true);
			animator.SetBool ("PlayerStandB", false);
			animator.SetBool ("PlayerRunning", false);
			animator.SetBool ("PlayerWalkB", false);
			iAmRunning = false;

			isRun = isRunMin;
		} 
		else if (playerHealthScript.damaged == false) 
		{
			animator.SetBool ("PlayerHit", false);
		}

		if (moveHorizontal == 0) 
		{
			animator.SetBool ("PlayerStandB", true);
			animator.SetBool ("PlayerRunning", false);
			animator.SetBool ("PlayerWalkB", false);
	
			iAmRunning = false;

			isRun = isRunMin;
		} 
		else if (Input.GetKey (KeyCode.Space) && canRunning == true)
		{
			Debug.Log ("i should be running now");
			iAmRunning = true;
			animator.SetBool ("PlayerRunning", true);
			animator.SetBool ("PlayerWalkB", false);
			animator.SetBool ("PlayerStandB", false);

			isRun += Time.deltaTime;
		}

		else if (iAmRunning == false && (moveHorizontal > 0 || moveHorizontal < 0))
		{
			animator.SetBool ("PlayerWalkB", true);
			animator.SetBool ("PlayerStandB", false);
			animator.SetBool ("PlayerRunning", false);
			iAmRunning = false;

			isRun -= Time.deltaTime;
		}  
	}

	void LayerCheck()
	{
		if(this.transform.position.y < 8.2f && this.transform.position.y > 6.2f)
		{
			mySpriteRenderer.sortingOrder = 9;
		}

		else if (this.transform.position.y >= 8f) 
		{
			mySpriteRenderer.sortingOrder = 6;
		}
		else if(this.transform.position.y <= 6.4f)
		{
			mySpriteRenderer.sortingOrder = 12;
		}
	}

	void Flip() // Method for changing the player objects facing
	{
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
		
}
