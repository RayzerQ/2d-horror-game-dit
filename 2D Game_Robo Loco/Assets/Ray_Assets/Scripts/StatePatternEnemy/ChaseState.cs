﻿using UnityEngine;
using System.Collections;

public class ChaseState : IenemyState {

	private readonly StatePatternEnemy enemy;
	private float hunting;
	private float huntingMin = 0;
	private float huntingMax = 1.3f;

	private float fieldOfViewAngle = 300f;

	public ChaseState (StatePatternEnemy statePatternEnemy)
	{
		enemy = statePatternEnemy;
	}


	public void UpdateState ()
	{
		Look ();
		Chase ();

		enemy.amPatroling = false;
		enemy.amAlerting = false;
		enemy.amChasing = true;
	}

	public void OnTriggerEnter (Collider other)
	{

	}

	public void OnTriggerStay (Collider other)
	{

	}

	public void ToPatrolState()
	{

	}

	public void ToAlertState()
	{
		hunting = huntingMin;
		enemy.currentState = enemy.alertState;
	}

	public void ToChaseState()
	{

	}


	private void Look()
	{
//		Vector3 direction = GameMaster.master.player.transform.position - enemy.transform.position;
//		float angle = Vector3.Angle(direction, enemy.transform.forward);
//
//		if (angle > fieldOfViewAngle * 0.5f) 
//		{
//			ToAlertState ();
//		} 
//		else if (angle < fieldOfViewAngle * 0.5f) 
//		{
//			RaycastHit hit;
//			Vector3 enemyToTarget = ((enemy.chaseTarget.position + enemy.offset) - enemy.eyes.transform.position);
//			//		Debug.DrawRay (enemy.eyes.transform.position, enemy.eyes.forward, Color.green, 30f);
//			if(Physics.Raycast (enemy.eyes.transform.position, enemyToTarget, out hit, enemy.sightRange) && hit.collider.CompareTag("Player"))
//			{
//			enemy.chaseTarget = hit.transform;
//			}
//		}
//
		Vector3 direction = GameMaster.master.player.transform.position - enemy.transform.position;
		float angle = Vector3.Angle(direction, enemy.transform.forward);

		if (angle < fieldOfViewAngle * 0.5f) 
		{
			RaycastHit hit;
			Vector3 enemyToTarget = ((enemy.chaseTarget.position + enemy.offset) - enemy.eyes.transform.position);
//			Debug.DrawRay (enemy.eyes.transform.position, enemy.eyes.forward, Color.green, 30f);

			if (Physics.Raycast (enemy.eyes.transform.position, enemyToTarget, out hit, enemy.sightRange) && hit.collider.CompareTag ("Player")) 
			{
				enemy.chaseTarget = hit.transform;
			} 
			else 
			{
				hunting += Time.deltaTime;
//				ToAlertState ();
			}
		}

		if (hunting >= huntingMax) 
		{
			ToAlertState ();
		}
	}

	private void Chase ()
	{
//		Debug.Log ("Chasing");
		enemy.meshRendererFlag.material.color = Color.red;
		enemy.navMeshAgent.destination = enemy.chaseTarget.position;
		enemy.navMeshAgent.Resume ();
	}
}