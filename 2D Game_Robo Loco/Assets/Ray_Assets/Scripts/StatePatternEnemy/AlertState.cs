﻿using UnityEngine;
using System.Collections;

public class AlertState : IenemyState {

	private readonly StatePatternEnemy enemy;
	private float searchTimer; 
	private float fieldOfViewAngle = 300f; 

	public AlertState (StatePatternEnemy statePatternEnemy)
	{
		enemy = statePatternEnemy;
	}

	public void UpdateState ()
	{
		Look ();
//		Search ();

		enemy.amPatroling = false;
		enemy.amAlerting = true;
		enemy.amChasing = false;
	}

	public void OnTriggerEnter (Collider other)
	{

	}

	public void OnTriggerStay (Collider other)
	{
		
	}

	public void ToPatrolState()
	{
		enemy.currentState = enemy.patrolState;
		searchTimer = 0f;
	}

	public void ToAlertState()
	{
		Debug.Log ("Cant transition to same state");
	}

	public void ToChaseState()
	{
		enemy.currentState = enemy.chaseState;
		searchTimer = 0f;
	}

	private void Look()
	{

//		// Create a vector from the enemy to the player and store the angle between it and forward.
		Vector3 direction = GameMaster.master.player.transform.position - enemy.transform.position;
		float angle = Vector3.Angle(direction, enemy.transform.forward);

		if (angle < fieldOfViewAngle * 0.5f) 
		{
			RaycastHit hit;
			if (Physics.Raycast (enemy.eyes.transform.position, enemy.eyes.transform.forward, out hit, enemy.sightRange) && hit.collider.CompareTag ("Player")) 
			{
				if (hit.distance < enemy.sightRange) 
				{
					enemy.chaseTarget = hit.transform;
					ToChaseState ();
				}
					
			}
		}

		searchTimer += Time.deltaTime;

		if (searchTimer >= enemy.searchingDuration) 
		{
			ToPatrolState ();
		}

		Debug.Log ("Searching");
		enemy.meshRendererFlag.material.color = Color.yellow;
		enemy.navMeshAgent.Stop ();
		enemy.transform.Rotate (0, enemy.searchingTurnSpeed * Time.deltaTime, 0);

	}	 
//		RaycastHit hit;
//		if (Physics.Raycast (enemy.eyes.transform.position, enemy.eyes.transform.forward, out hit, enemy.sightRange) && hit.collider.CompareTag ("Player")) 
//			{
//			enemy.chaseTarget = hit.transform;
//			ToChaseState ();
//		} 
//		else 
//		{
//			enemy.meshRendererFlag.material.color = Color.yellow;
//			enemy.navMeshAgent.Stop ();
//			enemy.transform.Rotate (0, enemy.searchingTurnSpeed * Time.deltaTime, 0);
//			searchTimer += Time.deltaTime;
//
//			if (searchTimer >= enemy.searchingDuration) 
//			{
//				ToPatrolState ();
//			}
//		}
	
}
