﻿using UnityEngine;
using System.Collections;

public class StatePatternEnemy : MonoBehaviour {

	public float searchingTurnSpeed;
	public float searchingDuration;
	public float sightRange;
	public Transform[] wayPoints;
	public Transform eyes;
	public Vector3 offset = new Vector3 (0, .5f, 0);
	public MeshRenderer meshRendererFlag;

	public bool amPatroling;
	public bool amAlerting;
	public bool amChasing;

	[HideInInspector] public Transform chaseTarget;
	[HideInInspector] public IenemyState currentState;
	[HideInInspector] public ChaseState chaseState;
	[HideInInspector] public PatrolState patrolState;
	[HideInInspector] public AlertState alertState;
	[HideInInspector] public NavMeshAgent navMeshAgent;

	private void Awake ()
	{
		chaseState = new ChaseState (this);
		alertState = new AlertState (this);
		patrolState = new PatrolState (this);

		navMeshAgent = GetComponent<NavMeshAgent> ();
	}


	void Start () 
	{
		currentState = patrolState;
	}
	

	void Update ()
	{
		currentState.UpdateState ();
	}

	private void OnTriggerEnter(Collider other)
	{
		currentState.OnTriggerEnter(other);
	}

	private void OnTriggerStay(Collider other)
	{
		currentState.OnTriggerEnter(other);
	}

}
