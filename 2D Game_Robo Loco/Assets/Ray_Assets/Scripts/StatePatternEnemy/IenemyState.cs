﻿using UnityEngine;
using System.Collections;

public interface IenemyState
{
	void UpdateState ();

	void OnTriggerEnter (Collider other);

	void OnTriggerStay (Collider other);

	void ToPatrolState();

	void ToAlertState();

	void ToChaseState();
}
