﻿using UnityEngine;
using System.Collections;

public class LevelComplete : MonoBehaviour {

	public GameObject player;

	private LevelLoader loader;

	void Awake()
	{
		player = GameObject.FindGameObjectWithTag ("Player");
		loader = GameMaster.master.GetComponent<LevelLoader> ();
	}

	void OnTriggerEnter (Collider other)
	{
		
		if(other.gameObject == player)
		{
			loader.LoadLevel ("WinScreen");
		}
	}
}