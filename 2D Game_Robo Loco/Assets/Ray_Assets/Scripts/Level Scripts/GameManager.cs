﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public GameObject player;
	public PlayerHealth playerHealthScript;

	public LevelLoader loader;

	public float identTime = 2f;

	void Awake()
	{
		player = GameObject.FindWithTag ("Player");
		playerHealthScript = player.GetComponent<PlayerHealth> ();


//		myLoader = GameMaster.master.GetComponent <LevelLoader> ();
	}

	void Start()
	{
		loader = GameMaster.master.GetComponent<LevelLoader> ();
//		Invoke ("GameOver", identTime);
	}

	void Update()
	{
		if (playerHealthScript.currentHealth <= 0) 
		{
			GameOver ();
		}
	}

	public void GameOver()
	{
//		SceneManager.LoadScene ("gameover");
		loader.LoadLevel("gameover");
		playerHealthScript.currentHealth = playerHealthScript.startingHealth;
		Debug.Log ("Should load game over scene now");
	}
}
