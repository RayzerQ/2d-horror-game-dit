﻿using UnityEngine;
using System.Collections;

public class Ident : MonoBehaviour {

	public float identTime = 2f; //  Declared a timer for the invoke action to wait

	private LevelLoader loader; // Declare instance of the LevelLoader script called loader


	void Start () 
	{
		loader = GameMaster.master.GetComponent<LevelLoader> (); // Sets loader variable with an action that finds the Gamesmaster script and digs down into it to get the LevelLoader script thats on it  
		//loader = GameObject.Find ("GameMaster").GetComponent<LevelLoader>(); // Alternate way of doing the above action

		Invoke ("GoToMenu", identTime); // Runs the below method after waiting the identTime
	}

	void GoToMenu()
	{
		loader.LoadLevel ("Menu"); // Having set the loader to be an instance of levellaoder script, runs that method within that script called Loadlevel with a string name "Menu"
	}

}
