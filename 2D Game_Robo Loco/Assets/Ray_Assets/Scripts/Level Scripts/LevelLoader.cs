﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement; // Essential for switching scenes in Unity 5

public class LevelLoader : MonoBehaviour {

	private bool loadingLevel = false; // Declared bool check for loading levels
	public string currentScene; // Declared string acting as a template for other scripts to put in the name of the level to be loaded

	void Awake ()
	{
		currentScene = SceneManager.GetActiveScene ().name.ToString (); // Puts the name of the level into the inspector
	}

	public void LoadLevel(string levelName) // This method load levels, other scripts can activate this and put the string of the desired level into the parameters
	{
		loadingLevel = false;
		StartCoroutine ("LoadingLevel", levelName); // Begins the coroutine with the parameters of the ienumerator name and the variable type
		Debug.Log ("Should load game over scene now");
	}

	IEnumerator LoadingLevel(string levelName) // Executes the routine defined above with the string parameter 
	{
		if (loadingLevel == false) // Safety check to prevent multiple levels being loaded at once
		{
			loadingLevel = true; // Sets the bool to true

			yield return new WaitForSeconds (2f); // Yield is necessary for the ienumerator, and the action will wait the time before the below action executes
			SceneManager.LoadScene (levelName); // Built in Unity function in Unity 5 for loading levels, loads the level with the string name passed down
		}
	}

	public void QuitToDesktop() // Method for quitting should be linked with a button in the menu scene
	{
		StartCoroutine ("Quit");
	}

	IEnumerator Quit()
	{
		yield return new WaitForSeconds (1f);
		Application.Quit ();
	}
}
