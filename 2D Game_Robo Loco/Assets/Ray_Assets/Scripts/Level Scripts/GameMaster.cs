﻿using UnityEngine;
using System.Collections;

public class GameMaster : MonoBehaviour {

	public bool dontDestroy = true; 
	public static GameMaster master; // Declared instance of this script

	public GameObject gameCamera; // Declared gameobject instance to be used for the camera
	public GameObject player; // Declared gameobject instance to be used for the player
	public CapsuleCollider playerCollider;

	void Start () 
	{
		master = this; // Sets the above declared variable as this script

		if (dontDestroy) // Checks if true
		{
			DontDestroyOnLoad (this.gameObject); // Makes sure this script is brought through into the next scene
		}
			 
	}
		
	void OnLevelWasLoaded() // Built in Unity method that runs once when a level is loaded
	{
		Debug.Log ("New Scene!");

		player = GameObject.FindWithTag ("Player"); // Sets the instance to the tagged player
		playerCollider = player.GetComponent<CapsuleCollider> ();

		CameraFind (); // Runs the camera find method, this sets a target in the camera script, which means it can be changed through code to something other than the player

	}

	public void CameraFind ()
	{
		gameCamera = GameObject.FindWithTag ("GameCamera"); // Sets the instance to the tagged camera

		if(player != null)
		{
			gameCamera.SendMessage("AssignTarget", player.transform, SendMessageOptions.RequireReceiver); // Now that the instance is stored, this runs the send message action to the object, checking through its components for a script with the "AssignTarget" method
		}
	}
}
