﻿using UnityEngine;
using System.Collections;

public class ForegroundScroller : MonoBehaviour {

	public float scrollSpeed;
	public float tileSizeZ;
	public GameObject player;
	public Robotcontroller robotControllerScript;
	public float playerDirection;

	private Vector3 currentPosition;

	void Start ()
	{
		player = GameObject.FindGameObjectWithTag ("Player");
		robotControllerScript = player.GetComponent<Robotcontroller> ();
	}

	void FixedUpdate ()
	{
		currentPosition = transform.position;
		playerDirection = robotControllerScript.moveHorizontal;

		if (playerDirection < 0) 
		{

			float newPosition = Mathf.Repeat (Time.deltaTime * scrollSpeed, tileSizeZ);
			transform.position = currentPosition + Vector3.right * newPosition;
		} 
		else if (playerDirection > 0) 
		{
			float newPosition = Mathf.Repeat (Time.deltaTime * scrollSpeed, tileSizeZ);
			transform.position = currentPosition + Vector3.left * newPosition;
		}
	}
}
