﻿using UnityEngine;
using System.Collections;

public class EnemySpriteLook : MonoBehaviour {

	public GameObject target;

	private bool facingRight = true;

	public Vector3 followpoint;
	private Vector3 currentPos;

	private SpriteRenderer mySpriteRenderer;

	Animator animator;

	public PatrolState patrolStating;
	public AlertState alertStating;
	public ChaseState chaseStating;

	private StatePatternEnemy statePatternEnemyScript;

//	public AudioClip patrolSound1;
//	public AudioClip alertSound1;
//	public AudioClip chaseSound1;


	void Awake ()
	{
		mySpriteRenderer = GetComponent<SpriteRenderer> ();
		animator = GetComponent <Animator> ();

		statePatternEnemyScript = target.GetComponent<StatePatternEnemy> ();
	}

	void Update()
	{

		LayerCheck ();

		currentPos = this.transform.position;
		followpoint = new Vector3 (target.transform.position.x, target.transform.position.y + 0.4f, target.transform.position.z);
		transform.position = Vector3.MoveTowards (transform.position, followpoint, 1f);

		if (this.transform.position.x < currentPos.x  && facingRight) 
		{
			FlipLeft();
		} 
		else if (this.transform.position.x > currentPos.x  && !facingRight) 
		{
			FlipRight();
		}
			
		if (statePatternEnemyScript.amPatroling == true) 
		{
//			AudioManager.instance.RandomizeSfx (patrolSound1, patrolSound2);
//			AudioManager.instance.PlaySingle (patrolSound1);
			animator.SetBool ("PatrolingYou", true);
			animator.SetBool ("AlertingYou", false);
			animator.SetBool ("ChasingYou", false);
		} 
		else if (statePatternEnemyScript.amAlerting == true) 
		{
//			AudioManager.instance.RandomizeSfx (alertSound1, alertSound2);
//			AudioManager.instance.PlaySingle (alertSound1);
			animator.SetBool ("AlertingYou", true);
			animator.SetBool ("PatrolingYou", false);
			animator.SetBool ("ChasingYou", false);
		}
		else if (statePatternEnemyScript.amChasing == true) 
		{
//			AudioManager.instance.RandomizeSfx (chaseSound1, chaseSound2);
//			AudioManager.instance.PlaySingle (chaseSound1);
			animator.SetBool ("ChasingYou", true);
			animator.SetBool ("PatrolingYou", false);
			animator.SetBool ("AlertingYou", false);
		}
	}

	void FlipLeft() // Method for changing the player objects facing
	{
		mySpriteRenderer.flipX = true;
		facingRight = false;
	}

	void FlipRight()
	{
		mySpriteRenderer.flipX = false;
		facingRight = true;
	}

	void LayerCheck()
	{
		if (this.transform.position.y < 8f && this.transform.position.y > 6.4f) 
		{
			mySpriteRenderer.sortingOrder = 9;
		} 
		else if (this.transform.position.y >= 8f) 
		{
			mySpriteRenderer.sortingOrder = 6;
		} 
		else if (this.transform.position.y <= 6.4f) 
		{
			mySpriteRenderer.sortingOrder = 12;
		}
	}
}
