﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	public Transform stillFocus;
	public Transform target; // Declared instance of target the camera looks at
	public float height; // Declared variable for height of camera
	public float damping = 10.0f; // Declared variable for rate that the camera follows behind target

	public float lineUp;

	Vector3 offset; // Stores a vector3 instance called offset for storing the vector3 gap between camera and target

	private Transform _myTransform; // Instance of the camera


	void Start ()  // At start if the target is empty then run the AssignTarget Method
	{
		if (target != null)
		{
			AssignTarget (target);
		}

		_myTransform = this.transform; // this caches the camera transform and means doesnt have to be looked up every time its moved

	}

	public void AssignTarget(Transform newTarget)
	{
		target = newTarget; // stores the assigned target in an instance

		offset = transform.position - newTarget.transform.position; // Defines what offset is
	}

	void LateUpdate()
	{
		if (target != null) // Checks for null
		{
			float desiredPositionX = target.transform.position.x; // stores a new vector3 that is the sum of the target ans offets vector3 values
			float desiredPositionY = target.transform.position.y;
			Vector3 desiredPosition = new Vector3(desiredPositionX, desiredPositionY + 6f, _myTransform.transform.position.z);
			_myTransform.position = Vector3.Lerp (_myTransform.position, desiredPosition, damping * Time.deltaTime);
			//Vector3 position = Vector3.Lerp (_myTransform.position, (0, desiredPosition, 0), Time.deltaTime * damping); //Stores another vector3 with the instruction to lerp from the camera to the above position at the damping rate
			//_myTransform.position = position; // clarifies that the camera instance (and thus the camera itself) is the same as the above vector3 with the lerp instruction
		}
	}
}
