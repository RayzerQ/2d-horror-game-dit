﻿using UnityEngine;
using System.Collections;

public class CameraUnhinge : MonoBehaviour {

	public GameObject player;
	public GameObject GameCamera;
	public Transform stillFocus;

	public bool unhinged = false;

	void Start()
	{
		GameCamera = GameObject.FindWithTag ("GameCamera");
		player = GameObject.FindWithTag ("Player");
	}
		
	public void OnTriggerEnter(Collider other)
	{
		Debug.Log ("Player Entered");

		GameCamera.SendMessage ("AssignTarget", stillFocus);
	}

	public void OnTriggerExit(Collider other)
	{
		GameCamera.SendMessage ("AssignTarget", player.transform);
	}
}
